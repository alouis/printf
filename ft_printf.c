/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 14:17:22 by alouis            #+#    #+#             */
/*   Updated: 2019/12/26 17:15:00 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft/libft.h"

int			ft_printf(const char *s, ...)
{
	t_printf		*res;
	size_t			ret;

	ret = 0;
	res = NULL;
	if (!(res = malloc(sizeof(t_printf))))
		return (0);
	va_start(res->ap, s);
	while (*s)
	{
		if (*s == '%')
		{
			s++;
			s = convert_param((char *)s, res);
			ret = ret + res->len;
		}
		else
		{
			ft_putchar(*s);
			ret++;
		}
		s++;
	}
	va_end(res->ap);
	free(res);
	return (ret);
}

const char	*convert_param(char *str, t_printf *res)
{
	res = ft_initialize(res);
	while (*str == '0' || *str == '-')
	{
		if (*str == '0')
			ft_zero_flag(&str, res);
		if (*str == '-')
			ft_flags(&str, res);
	}
	if (*str != '0' && *str != '-' && (ft_isdigit(*str) || *str == '*'))
		ft_width(&str, res);
	if (*str == '.')
	{
		res->prec_flag = 1;
		ft_precision(&str, res);
	}
	if (ft_strchr("cspdiuxX%", *str))
	{
		handle_param(*str, res);
		return (str);
	}
	return (str);
}

int			handle_param(char c, t_printf *res)
{
	get_type(c, res);
	if (res->type == INT)
		print_int(res, va_arg(res->ap, int));
	if (res->type == STRING)
		print_str(res, va_arg(res->ap, char *));
	if (res->type == CHAR && c != '%')
		print_char(res, (char)va_arg(res->ap, int));
	if (res->type == CHAR && c == '%')
		print_char(res, '%');
	if (res->type == UNSIGNED)
		print_unsigned(res, va_arg(res->ap, unsigned int));
	if (res->type == POINTER)
		print_ptr(res, va_arg(res->ap, unsigned long));
	if (res->type == HEXA || res->type == MAJ)
		print_hexa(res, va_arg(res->ap, unsigned int));
	return (1);
}

void		get_type(char c, t_printf *res)
{
	if (c == 'd' || c == 'i')
		res->type = INT;
	if (c == 's')
		res->type = STRING;
	if (c == 'c' || c == '%')
		res->type = CHAR;
	if (c == 'p')
		res->type = POINTER;
	if (c == 'u')
		res->type = UNSIGNED;
	if (c == 'x')
		res->type = HEXA;
	if (c == 'X')
		res->type = MAJ;
	return ;
}
