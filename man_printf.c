#include <stdio.h>

int	main()
{
	int n = -10;
	int m = 500;
	int h = 23;
	int a = 1994;
	int	i = 3;
	int x = 6;
	int j = 5;
	int z = 0;
	int mj = -5;
	unsigned int u = 3258594758;
	long int l = 3258594758;
	double	d = 23.56;
	double	f = 23;
	char c = '\0';
	char s[] = "hello world !";
	char e[] = "world !";
	char empty[] = "";
	int ret;

	printf("-------------\n-------------\n| FT_PRINTF |\n-------------\n-------------\n\n");

	printf("! return value = lenght of (char *) printed !\n");
	printf("ex :");
	ret = printf("four");
	printf("\n");
	printf("ret printf(\"four\") = %d\n", ret);
	printf("\n");
	printf("! if printfi(), too few arg to function call !\n");
	printf("\n");
	printf("! printf doesn't count last '\\0', if arg = '\\0', ret counts it !\n");
	printf("printf(\"\") = '\\0'\n");
	ret = 0;
	ret = printf("%c", c);
	printf("ret = %d\n", ret);
	ret = 0;
	printf("for char c = '\\0' printf(\"!%%c!\") = ");
	ret = printf("!%c!", c);
	printf("\n");
	printf("ret = %d\n", ret);
	printf("\n\n");

/*	printf("\n\n\n");
	printf("CHARACTERS ESCAPE SEQUENCES\n***************************\n\n");
	printf("bell (\\a)           : 123456 = 123\a456\n");
	printf("backspace (\\b)      : 12_45_7_9 = 123\b456\b78\b9\n");
	printf("form-feed char (\\f) : 123456 = 123\f456\n");
	printf("car carriage (\\r)   : 123456 = 123\r456\n");
	printf("tab (\\t)            : 123456 = 123\t456\n");
	printf("vertical tab (\\v)   : 123456 = 123\v456\n\n");
	printf("!! one doesn't work after another !!\n");
*/
	printf("\n\n\n");
	printf("FLAGS CHARACTERS\n****************\n\n");
/*	printf("-----\n| # | no effect on c, d, i, n, p, s and u conversions. Adds a zero before the outpu (except for g ?)\n-----\n"); 
	printf("octo (o) + #   : -10 = %#o ; 500 = %#o\n\n", n, m);	
	printf("hexa (x) + #   : -10 = %#x ; 500 = %#x\n\n", n, m);	
	printf("double (a) + # : 23.56 = %#a\n\n", d);
	printf("double (g)     : 23.56 = %g ; 23 = %g\n", d, f);
	printf("double (g) + # : 23.56 = %#g ; 23 = %#g\n\n", d, f);
*/
	printf("-----\n| 0 | returned value padded on the left with zeros rather than blanks (overrided by '-') \n-----\n"); 
	printf("int (d)         : -10 = %d ; 500 = %d\n", n, m);	
	printf("int (d) + 05    : -10 = %05d ; 500 = %05d\n\n", n, m);	
	printf("int (d) + 0*    : -10 = %0*d ; 500 = %0*d\n\n", j, n, j, m);	
	printf("int (d) + 0000  : -10 = %0000d ; 500 = %0000d\n\n", n, m);	
	printf("octo (o)        : -10 = %o ; 500 = %o\n", n, m);	
	printf("octo (o) + 010  : -10 = %010o ; 500 = %010o\n\n", n, m);	
	printf("hexa (x)        : -10 = %x ; 500 = %x\n", n, m);	
	printf("hexa (x) +  02  : -10 = %03x ; 500 = %03x\n\n", n, m);	
	printf("double (a)      : 23 = %a\n", f);	
	printf("double (a) + 0  : 23 = %0a\n\n", f);
	printf("int (d) + 03    : -10 = %03d ; 500 = %03d\n", n, m);	
	printf("int (d) + 04    : -10 = %04d ; 500 = %04d\n\n", n, m);	
	printf("int (d) + 010-5 : -10 = %010-5d ; 500 = %010-5d\n\n", n, m);	
	printf("char *(s) + 03  : world ! = %03s\n", e);	
	printf("char *(s) + 015 : world ! = %015s\n\n", e);	
	printf("!! %%0nd where  n = ret !!\n");
	printf("!! if arg < 0, (-) sould appear before the zeros and counts as 1 in the total lenght !!\n");
	printf("!! undefined behavior with c and s conversions !!\n\n\n");
	
	printf("-----\n| - | fills space left with n - len blanks (overrides '0') \n-----\n"); 
	printf("int (d) + -3    : -10! = %-3d! ; 500! = %-3d!\n", n, m);	
	printf("int (d) + -4    : -10! = %-4d! ; 500! = %-4d!\n\n", n, m);	
	printf("int (d) + -*    : -10! = %-*d! ; 500! = %-*d!\n", i, n, j, m);	
	printf("int (d) + -*    : -10! = %-*d! ; 500! = %-*d!\n\n", j, n, j, m);	
	printf("octo (o) + -5   : -10! = %-5o! ; 500! = %-5o!\n", n, m);	
	printf("octo (o) + -13  : -10! = %-13o! ; 500! = %-13o!\n\n", n, m);	
	printf("hexa (x) + -6   : -10! = %-6x! ; 500! = %-6x!\n", n, m);	
	printf("hexa (x) +  -9  : -10! = %-9x! ; 500! = %-9x!\n\n", n, m);	
	printf("double (a) + -20: 23.56! = %-20a! ; 23! = %-20a!\n", d, f);	
	printf("double (a) + -9 : 23.56! = %-9a! ; 23! = %-9a!\n\n", d, f);
	printf("char* (s) + -50 : worldworld = %-50sworld\n\n", e);	
	printf("char (c) + -50  : aa = %-50ca\n\n", c);	

/*	printf("-----\n| + | only for signed formats, should always have a sign before the number \n-----\n"); 
	printf("int (d) + +          : -10 = %+d ; 500 = %+d\n", n, m);	
	printf("double (a) + +       : 23.56 = %+a ; 23 = %+a\n", d, f);	
	printf("long (ld) + +        : int max ++ = %+ld\n\n", l);
	printf("!! undefined behavior with x, u and o conversions !!\n\n\n");

	printf("---------\n| space | blank left before positive number or empty string produced by signed conversion\n---------\n");
	printf("int (d) + space    : 500 = % d\n", n);	
	printf("double (a) + space : 23.56 = % a ; 23 = % a\n", d, f);	
	printf("long (ld) + space  : int max ++ = % ld\n", l);
	printf("!! undefined behavior with s and c conversions !!\n\n");

	printf("-----\n| ' | fills n - len blanks before the output (a '+' overrides a space)\n-----\n"); 
	printf("int (d) + '50        : 1994 = %'50d\n", a);
	printf("int (i) + '6         : 1994 = %'6i\n", a);
	printf("unsigned int (u) + ' : int max ++ = %'u\n", u);
	printf("double (f) + '25     : 23.56 = %'25f\n\n", d);
	printf("!! undefined behavior with x, o and a conversions !!\n\n");
*/
	printf("\n\n\n");
	printf("FIELD WIDTH\n***********\n\n");
	printf("----------------\n| digit string | padding = len - n \n----------------\n"); 
	printf("int (d) + 98  : 23 = !!%98d!!\n", h);
	printf("int (d) + 98* : 23 = !!%*d!!\n", h, h);
	printf("int (d) + 98* : 23 = !!%98*d!!\n", h, h);
	printf("int (d) + 98- : 23 = !!%98-d!!\n", h);
	printf("char* (s) + 7 : hello world ! = %7s\n", s);
	printf("char (c) + 25 : a = %25c\n\n", c);
	printf("-----\n| * | replace digit string by argument \n-----\n");
	printf("int (d) + *   : 1994 = %*d\n", h, a);
	printf("char* (s) + * : hello world ! = %*s\n", h, s);
	printf("char (c) + *  : a = %*c\n", h, c);

	printf("\n\n\n");
	printf("PRECISION\n*********\n\n");
	printf("-----\n| . | padding on the left with zero except with s conversion\n-----\n"); 
	printf("int (d) + .3    : 1994 = %.3d\n", a);
	printf("int (d) + .5    : 1994 = %.5d\n\n", a);
	printf("hexa (x) + .2  	: 1994 = %.2x\n", a);
	printf("hexa (x) + .6   : 1994 = %.6x\n\n", a);
	printf("char* (s) + .3  : world ! = %.3s\n", e);
	printf("char* (s) + .15 : world ! = %.15s\n\n", e);
	printf("!! undefined behavior with c conversion !!\n");

	printf("\n\n\n");
	printf("-----\n| * | replace digit string by argument \n-----\n");
	printf("int (d) + .*   : 1994 = %.*d\n", h, a);
	printf("int (d) + .*   : 1994 = %.*d\n", z, a);
	printf("int (d) + .*   : 1994 = %.*d\n", 1, 0);



	printf("\n\n\n");
	printf("FORMAT CONVERSION\n**************\n\n");
	printf("Nbr (d or i) : %d \nUnsigned (u) : %u \nLong (ld)    : %ld \nDouble (a)   : %a\n", a, u, l, d);
	printf("For nbr (d) = %d ; Unsigned (u) : %u ; Long (ld) = %ld\n\n", n, n, n);
	printf("octo (o)       : -10 = %o ; 500 = %o\n", n, m);
	printf("1994 in octal is %o\n\n", a);
	printf("hexa (x)       : -10 = %x ; 500 = %x\n", n, m);	
	printf("1994 in hexa is %x\n\n", a);
	printf("pointer (p) : %p\n\n", c);


	printf("!! INVALID FORMAT !!\n\n");
	printf("%%%%4.c = ");
	printf("%%4.c");
	printf("\n\n");
	printf("uu%% = uu%");
	printf("\n\n");
	printf("uu%%abcdef = uu%abcdef	//if no arg given, replace %%a by anything");
	printf("\n\n");
	printf("uu%%0123456789 = uu %0123456789");
	printf("\n\n\n");
	
	printf("!! COMBINED FLAGS !!\n\n");
	printf("0 + . + digit = 0 + digit = . + digit\n");
	printf("hello%%05d     = hello%05d\n", h);
	printf("hello%%0.5d    = hello%0.5d\n", h);
	printf("hello%%.5d     = hello%.5d\n", h);
	printf("hello%%0.-5d   = hello%0.-5d!\n", h);
	printf("hello%%03.-5d  = hello%03.*d!\n", -5, h);
	printf("hello%%05.-5d  = hello%05.*d!\n", -5, h);
	printf("hello%%07.-5d  = hello%07.*d!\n", -5, h);
	printf("hello%%0-5.-5d = hello%0-5.*d!\n", -5, h);
	printf("hello%%0-7.-5d = hello%0-7.*d!\n", -5, h);
	printf("hello%%-7.-5d  = hello%-7.*d!\n", -5, h);
	printf("hello%%-3.-5d  = hello%-3.*d!\n", -5, h);

	printf("\n\n");
	printf("!! if several values for . only the first counts. If several . last . counts as value. !!\n");
	printf("hello%%.5*15d        = hello%.5*15d!\n", n, h);
	printf("hello%%.15*5d        = hello%.15*5d!\n", n, h);
	printf("hello%%.5*60.7d      = hello%.5*60.7d!\n", n, h);
	printf("hello%%.-5*15d       = hello%.-5*15d!\n", n, h);
	printf("hello%%.15*5.-7*d    = hello%.15*5.-7*d!\n", n, 15, h);
	printf("hello%%0.15*5.-7d    = hello%0.15*5.-7d!\n", n, h);
	printf("hello%%.15*5.-7*d    = hello%.15*5.-7*d!\n", n, 10, h);
	printf("hello%%.15*5.7*d     = hello%.15*5.7*d!\n", n, 10, h);
	printf("hello%%.15*5.7-7*d   = hello%.15*5.7*d!\n", n, 10, h);
	printf("hello%%0.15*5.7-7*d  = hello%0.15*5.7*d!\n", n, 10, h);

	

	printf("\n\n");
	printf("!! width or prec or both < 0, equals - flag. prec overrides width !!\n");
	printf("hello%%-12d  = hello%-12d!\n", h);
	printf("hello%%.-12d  = hello%.-12d!\n", h);
	printf("hello%%-12.-12d  = hello%-12.-12d!\n", h);
	printf("hello%%-12.-20d  = hello%-12.-20d!\n", h);
	printf("hello%%-20.-12d  = hello%-20.-12d!\n", h);

	printf("\n\n");
	printf("!! if 0 then .* where * < 0, prec padds right (acts as -) !!"); 
	printf("\n\n");
	printf("width + prec = first prec then width\n");
	printf("!world !! 4.3 = !%4.3s!\n", e);
	printf("!world !! 4.1 = !%4.1s!\n", e);
	printf("!world !! 4.5 = !%4.5s!\n", e);
	printf("!world !! 6.5 = !%6.5s!\n\n", e);
	printf("!1994! 7.5  = !%7.5d!\n", a);
	printf("!1994! 3.7  = !%3.7d!\n", a);
	printf("\n\n");
	printf("for arg = 23 then 10 or 10 then 23.\n");
	printf("int + -** = !%-**d!\n", h, n, a);
	printf("int + -** = !%-**d!\n", h, 10, a);
	printf("int + -** = !%-**d!\n", n, h, a);
	printf("\n\n");
	printf("!last indication has the priority. ex : * = 23, digit = 10!\n");
	printf("digit + * !world !!  = !%010*s!\n", h, e);
	printf("* + digit !world !!  = !%0*10s!\n", h, e);
	printf("digit + * !world !!  = !%010*d!\n", h, a);
	printf("* + digit !world !!  = !%0*10d!\n", h, a);
	printf("\n\n");
	printf("!world !! 6.5 = !%0*.*s!\n", x, j, e);	
	printf("\n\n");
	printf("! with u, d, and i, combined 0 and .!\n");

	printf("\n\n");
	printf("! when * < 0, not the same behavior with 0 and -. With -, * < 0, * = * x -1 !\n");
	printf("int + -* w/ arg < 0   = !%-*d!\n",  n, a);
	printf("int + -* w/ arg *= -1 = !%-*d!\n", 10, a);
	printf("int + -0 w/ arg < 0   = !%0*d!\n",  n, a);
	printf("int + -0 w/ arg *= -1 = !%0*d!\n", 10, a);

	printf("\n\n");
	printf("! - and prec, combined but precision first !\n");
	printf("-30.30d = !%-30.30d!\n", h);
	printf("-31.30d = !%-31.30d!\n", h);
	printf("-32.30d = !%-32.30d!\n", h);
	printf("-40.30d = !%-40.30d!\n", h);
}
