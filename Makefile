# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alouis <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/11/29 10:31:36 by alouis            #+#    #+#              #
#    Updated: 2019/12/16 16:07:34 by alouis           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		=	libftprintf.a

LIBFT_A 	=	libft.a

LIBFT_DIR	=	libft

SRCS		=	ft_printf.c \
				ft_init_free.c \
				ft_handle_flags.c \
				ft_print_chars.c \
				ft_print_nbr.c \
				ft_print_hexa.c \
				ft_print_unsigned.c

OBJS		=	$(SRCS:.c=.o)

CC			=	gcc

FLAGS		=	-Wall -Wextra -Werror

.c.o:		${SRCS} 
			${CC} ${FLAGS} -Ilibft -c $< -o ${<:.c=.o}

all:		${NAME}

$(LIBFT_A):	
			make -C ${LIBFT_DIR}
			cp ${LIBFT_DIR}/${LIBFT_A} ${NAME}

$(NAME):	${LIBFT_A} ${OBJS}
			ar -rcs ${NAME} ${OBJS}

clean:
			rm -f ${OBJS}
			make clean -C ${LIBFT_DIR}

fclean:		clean
			rm -f ${NAME}
			make fclean -C ${LIBFT_DIR}

re:			fclean all
