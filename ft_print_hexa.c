/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_hexa.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 16:48:14 by alouis            #+#    #+#             */
/*   Updated: 2019/12/19 14:14:04 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

void	width_hex(t_printf *res)
{
	if (res->width <= res->prec || res->width <= res->str_len)
		res->width = 0;
	else if (res->width >= res->prec && res->prec >= res->str_len)
		res->width = res->width - res->prec;
	else if (res->width > res->str_len)
		res->width = res->width - res->str_len;
	while (res->width)
	{
		ft_putchar(' ');
		res->width--;
		res->len++;
	}
}

void	pad_left_hex(t_printf *res)
{
	int i;

	i = 0;
	if (res->prec > res->str_len)
	{
		while (i < (res->prec - res->str_len))
		{
			ft_putchar('0');
			i++;
			res->len++;
		}
	}
	if (res->pad_left > res->str_len)
	{
		while (i < (res->pad_left - res->str_len))
		{
			ft_putchar('0');
			i++;
			res->len++;
		}
	}
}

void	pad_right_hex(t_printf *res)
{
	if (res->prec <= res->str_len)
		res->pad_right = res->pad_right - res->str_len;
	else if (res->pad_right > res->prec)
		res->pad_right = res->pad_right - res->prec;
	else
		res->pad_right = 0;
	while (res->pad_right)
	{
		ft_putchar(' ');
		res->pad_right--;
		res->len++;
	}
}

void	print_hexa(t_printf *res, unsigned int hex)
{
	if (res->prec_flag == 1 && res->prec == 0 && hex == 0)
		res->str = ft_strdup("\0");
	else
		res->str = ft_itoa_base((unsigned long)hex, res);
	res->str_len = (int)ft_strlen(res->str);
	if (res->zero_flag == 1 && res->prec_flag == 1)
	{
		res->width = res->pad_left;
		res->pad_left = 0;
		width_hex(res);
	}
	if (res->width > res->str_len)
		width_hex(res);
	if (res->prec || res->pad_left)
		pad_left_hex(res);
	res->len = res->len + ft_putstr(res->str);
	if (res->pad_right > res->str_len)
		pad_right_hex(res);
	res->str = free_str(res->str);
}

void	print_ptr(t_printf *res, unsigned long int ptr)
{
	if (res->prec_flag == 1 && res->prec == 0 && ptr == 0)
		res->str = ft_strdup("\0");
	else
		res->str = ft_itoa_base(ptr, res);
	res->str_len = ft_strlen(res->str) + 2;
	if (res->zero_flag == 1 && res->prec_flag == 1)
	{
		res->width = res->pad_left;
		res->pad_left = 0;
		width_hex(res);
	}
	if (res->width > res->str_len)
		width_hex(res);
	res->len = res->len + ft_putstr("0x");
	if (res->prec || res->pad_left)
		pad_left_hex(res);
	res->len = res->len + ft_putstr(res->str);
	if (res->pad_right > res->str_len)
		pad_right_hex(res);
	res->str = free_str(res->str);
}
