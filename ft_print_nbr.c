/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_nbr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 15:23:01 by alouis            #+#    #+#             */
/*   Updated: 2019/12/18 18:55:26 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

void	width_nbr(t_printf *res, int n)
{
	if (res->width > res->prec && res->prec >= (int)ft_strlen(res->str))
	{
		res->width = res->width - res->prec;
		if (n < 0 && res->zero_flag == 0)
			res->width--;
	}
	else if (res->width > (int)ft_strlen(res->str) && res->width > res->prec)
		res->width = res->width - (int)ft_strlen(res->str);
	else if (res->width >= (int)ft_strlen(res->str) &&
			res->width > res->prec && n < 0)
	{
		if (res->prec == 0 && res->prec_flag == 1)
			res->width = res->width - (int)ft_strlen(res->str);
		else
			res->width = res->width - (int)ft_strlen(res->str) + 1;
	}
	else if (res->width <= res->prec || res->width <= (int)ft_strlen(res->str))
		res->width = 0;
	while (res->width)
	{
		ft_putchar(' ');
		res->width--;
		res->len++;
	}
}

void	pad_left_nbr(t_printf *res)
{
	int i;

	i = 0;
	if (res->prec > (int)ft_strlen(res->str))
	{
		while (i < (res->prec - (int)ft_strlen(res->str)))
		{
			ft_putchar('0');
			i++;
			res->len++;
		}
	}
	if (res->pad_left > (int)ft_strlen(res->str))
	{
		while (i < (res->pad_left - (int)ft_strlen(res->str)))
		{
			ft_putchar('0');
			i++;
			res->len++;
		}
	}
}

void	pad_right_nbr(t_printf *res, int n)
{
	if (res->prec <= (int)ft_strlen(res->str))
		res->pad_right = res->pad_right - (int)ft_strlen(res->str);
	else if (res->pad_right > res->prec)
		res->pad_right = res->pad_right - res->prec;
	else
		res->pad_right = 0;
	while (res->pad_right && n >= 0)
	{
		ft_putchar(' ');
		res->pad_right--;
		res->len++;
	}
	while (res->pad_right > 1 && n < 0)
	{
		ft_putchar(' ');
		res->pad_right--;
		res->len++;
	}
}

void	print_neg(t_printf *res, int n)
{
	unsigned int nb;

	nb = 0;
	ft_putchar('-');
	res->str = free_str(res->str);
	n *= -1;
	res->str = ft_itoa(n);
	if (n == -2147483648)
	{
		nb = n * -1;
		res->str = ft_itoa_unsigned(nb);
	}
	res->len++;
	if (res->pad_left > ((int)ft_strlen(res->str)))
		res->pad_left--;
}

void	print_int(t_printf *res, int n)
{
	res->str = ft_itoa(n);
	if (res->prec_flag == 1 && res->prec == 0 && n == 0)
		res->str = ft_strdup("\0");
	if (res->zero_flag == 1 && res->prec_flag == 1)
	{
		res->width = res->pad_left;
		if ((n < 0 && res->width == (int)ft_strlen(res->str) &&
			res->prec > 0) || (n < 0 && res->width > res->prec &&
			res->prec >= (int)ft_strlen(res->str)))
			res->width--;
		res->pad_left = 0;
		width_nbr(res, n);
	}
	if (res->width > (int)ft_strlen(res->str))
		width_nbr(res, n);
	if (n < 0)
		print_neg(res, n);
	if (res->prec || res->pad_left)
		pad_left_nbr(res);
	res->len = res->len + ft_putstr(res->str);
	if (res->pad_right > (int)ft_strlen(res->str))
		pad_right_nbr(res, n);
	res->str = free_str(res->str);
}
