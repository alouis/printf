/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_handle_flags.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/10 18:58:46 by alouis            #+#    #+#             */
/*   Updated: 2019/12/18 17:52:16 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft/libft.h"

void	ft_zero_flag(char **str, t_printf *res)
{
	res->zero_flag = 1;
	while (**str == '0')
		(*str)++;
	if (ft_isdigit(**str) == 1)
	{
		res->pad_left = ft_atoi(*str);
		while (ft_isdigit(**str))
			(*str)++;
	}
	if (ft_is_arg(**str, res))
	{
		res->pad_left = res->arg;
		(*str)++;
	}
}

void	ft_flags(char **str, t_printf *res)
{
	res->pad_left = 0;
	res->min_flag = 1;
	while (**str == '-')
		(*str)++;
	if (ft_isdigit(**str))
	{
		res->pad_right = ft_atoi(*str);
		while (ft_isdigit(**str))
			(*str)++;
	}
	if (ft_is_arg(**str, res))
	{
		res->pad_right = res->arg;
		(*str)++;
	}
}

int		ft_is_arg(char c, t_printf *res)
{
	if (c == '*')
	{
		res->arg = va_arg(res->ap, int);
		if (res->arg < 0)
		{
			if (res->width == 0 && res->prec_flag == 0)
				res->pad_right = res->arg * -1;
			if (res->width > 0 && res->prec_flag == 1)
				res->prec_flag = 0;
			if (res->min_flag == 1 && res->prec == 0)
				res->arg *= -1;
			if ((res->min_flag == 1 && res->prec_flag == 1) ||
				(res->width == 0 && res->prec_flag == 1))
			{
				res->prec_flag = 0;
				res->arg = 0;
			}
		}
		return (1);
	}
	return (0);
}

void	ft_width(char **str, t_printf *res)
{
	if (ft_isdigit(**str))
	{
		res->width = ft_atoi(*str);
		while (ft_isdigit(**str))
			(*str)++;
	}
	if (ft_is_arg(**str, res))
	{
		res->width = res->arg;
		(*str)++;
	}
}

void	ft_precision(char **str, t_printf *res)
{
	res->prec_flag = 1;
	(*str)++;
	if (ft_isdigit(**str))
	{
		res->prec = ft_atoi(*str);
		while (ft_isdigit((**str)))
			(*str)++;
	}
	if (ft_is_arg(**str, res))
	{
		res->prec = res->arg;
		(*str)++;
	}
}
