/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_free.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/06 13:53:02 by alouis            #+#    #+#             */
/*   Updated: 2019/12/18 17:53:59 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_printf	*ft_initialize(t_printf *res)
{
	res->c = 0;
	res->str = NULL;
	res->len = 0;
	res->str_len = 0;
	res->pad_left = 0;
	res->zero_flag = 0;
	res->pad_right = 0;
	res->min_flag = 0;
	res->arg = 0;
	res->width = 0;
	res->prec = 0;
	res->prec_flag = 0;
	res->mlc = 0;
	return (res);
}

char		*free_str(char *str)
{
	if (str != NULL)
	{
		free(str);
		str = NULL;
	}
	return (str);
}
