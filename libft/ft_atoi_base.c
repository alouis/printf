/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/18 19:09:24 by alouis            #+#    #+#             */
/*   Updated: 2019/12/18 19:10:00 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

static int		ft_space(char c)
{
	if (c == 32 || c == '\t' || c == '\v' || c == '\f' || c == '\r'
			|| c == '\n')
		return (1);
	return (0);
}

static int		ft_strlen(char *str)
{
	int len;

	len = 0;
	while (str[len])
	{
		len++;
	}
	return (len);
}

static int		ft_check_base(char *base)
{
	int i;
	int j;

	i = 0;
	if (ft_strlen(base) == 0 || ft_strlen(base) == 1 || !(base[i]))
		return (0);
	while (base[i])
	{
		if (base[i] == '+' || base[i] == '-')
			return (0);
		else if (base[i] < 32 || base[i] > 126)
			return (0);
		j = i + 1;
		while (base[j])
		{
			if (base[i] == base[j])
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

static int		get_index(char c, char *base)
{
	int i;

	i = 0;
	while (base[i])
	{
		if (c == base[i])
			return (i);
		i++;
	}
	return (-1);
}

int				ft_atoi_base(char *str, char *base)
{
	int i;
	int neg;
	int res;

	i = 0;
	neg = 1;
	res = 0;
	if (ft_check_base(base))
	{
		while (ft_space(str[i]) == 1 && str[i])
			i++;
		while ((str[i] == '+' || str[i] == '-') && str[i])
		{
			if (str[i] == '-')
				neg *= -1;
			i++;
		}
		while (get_index(str[i], base) != -1 && str[i])
		{
			res = res * ft_strlen(base) + get_index(str[i], base);
			i++;
		}
	}
	res = res * neg;
	return (res);
}
