/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/10 10:37:31 by alouis            #+#    #+#             */
/*   Updated: 2019/12/18 14:47:52 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include "../ft_printf.h"

size_t	ft_strlen(const char *s);
int		ft_strchr(char *s, char c);
int		ft_putstr(char *s);
int		ft_putnstr(char *s, int len);
void	ft_putnchar(char c, int len);
void	ft_putchar(char c);
char	*ft_itoa(int n);
int		ft_isdigit(int c);
int		ft_atoi(char *s);
char	*ft_itoa_base(unsigned long int n, t_printf *res);
char	*ft_itoa_unsigned(unsigned int n);
char	*ft_strdup(const char *base);

#endif
