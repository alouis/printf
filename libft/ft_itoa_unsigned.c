/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_unsigned.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/16 16:04:55 by alouis            #+#    #+#             */
/*   Updated: 2019/12/18 19:08:01 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

static size_t	size(unsigned int n)
{
	size_t	len;

	len = 0;
	if (n == 0 || n < 0)
	{
		len++;
		n *= -1;
	}
	while (n)
	{
		len++;
		n = n / 10;
	}
	return (len);
}

char			*ft_itoa_unsigned(unsigned int n)
{
	char			*a;
	size_t			len;

	len = size(n);
	if (!(a = (char *)malloc(sizeof(char) * len + 1)))
		return (NULL);
	a[len--] = '\0';
	if (n >= 0)
	{
		a[len] = n + 48;
	}
	while (n)
	{
		a[len--] = n % 10 + 48;
		n = n / 10;
	}
	return (a);
}
