/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 17:05:38 by alouis            #+#    #+#             */
/*   Updated: 2019/12/18 19:07:41 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "../ft_printf.h"
#include <stdio.h>

static size_t	size(unsigned long int n)
{
	size_t len;

	len = 0;
	if (n == 0)
		len++;
	while (n)
	{
		len++;
		n = n / 16;
	}
	return (len);
}

char			*ft_strdup(const char *base)
{
	char	*cpy;
	int		i;

	i = 0;
	if (!(cpy = malloc(sizeof(char) * 17)))
		return (NULL);
	while (base[i])
	{
		cpy[i] = base[i];
		i++;
	}
	cpy[i] = '\0';
	return (cpy);
}

static char		convert_hex(unsigned long int index, t_printf *res)
{
	char *base;
	char tmp;

	if (res->type == HEXA || res->type == POINTER)
		base = ft_strdup("0123456789abcdef");
	if (res->type == MAJ)
		base = ft_strdup("0123456789ABCDEF");
	tmp = base[index];
	base = free_str(base);
	return (tmp);
}

char			*ft_itoa_base(unsigned long int n, t_printf *res)
{
	char				*a;
	unsigned long int	tmp;
	size_t				len;

	len = size(n);
	if (!(a = malloc(sizeof(char) * len + 1)))
		return (NULL);
	a[len--] = '\0';
	if (n == 0)
		a[len] = 48;
	while (n > 0)
	{
		tmp = n % 16;
		n = n / 16;
		a[len--] = convert_hex(tmp, res);
	}
	return (a);
}
