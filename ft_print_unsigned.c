/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_unsigned.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/16 15:48:12 by alouis            #+#    #+#             */
/*   Updated: 2019/12/19 14:05:57 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft/libft.h"

void	print_unsigned(t_printf *res, unsigned int n)
{
	if (res->prec_flag == 1 && res->prec == 0 && n == 0)
		res->str = ft_strdup("\0");
	else
		res->str = ft_itoa_unsigned(n);
	res->str_len = ft_strlen(res->str);
	if (res->zero_flag == 1 && res->prec_flag == 1)
	{
		res->width = res->pad_left;
		res->pad_left = 0;
		width_hex(res);
	}
	if (res->width > res->str_len)
		width_hex(res);
	if (res->prec || res->pad_left)
		pad_left_hex(res);
	res->len = res->len + ft_putstr(res->str);
	if (res->pad_right > res->str_len)
		pad_right_hex(res);
	res->str = free_str(res->str);
}
