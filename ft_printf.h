/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/29 17:18:56 by alouis            #+#    #+#             */
/*   Updated: 2019/12/19 14:06:09 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdio.h>

typedef enum	e_list
{
	CHAR, STRING, INT, UNSIGNED, POINTER, HEXA, MAJ
}				t_list;

typedef struct	s_printf
{
	va_list		ap;
	char		c;
	char		*str;
	int			len;
	int			str_len;
	t_list		type;
	int			pad_left;
	int			zero_flag;
	int			pad_right;
	int			min_flag;
	int			arg;
	int			width;
	int			prec;
	int			prec_flag;
	int			mlc;
}				t_printf;
int				ft_printf(const char *s, ...);
const char		*convert_param(char *str, t_printf *res);
void			ft_zero_flag(char **str, t_printf *res);
void			ft_flags(char **str, t_printf *res);
int				ft_is_arg(char c, t_printf *res);
void			ft_width(char **str, t_printf *res);
void			ft_precision(char **str, t_printf *res);
void			get_type(char c, t_printf *res);
int				handle_param(char c, t_printf *res);
void			print_str(t_printf *res, char *s);
void			print_char(t_printf *res, char c);
void			width_str(t_printf *res);
void			pad_right_str(t_printf *res);
void			print_int(t_printf *res, int n);
void			print_unsigned(t_printf *res, unsigned int n);
void			print_ptr(t_printf *res, unsigned long ptr);
void			width_nbr(t_printf *res, int n);
void			pad_left_nbr(t_printf *res);
void			pad_right_nbr(t_printf *res, int n);
void			print_neg(t_printf *res, int n);
void			print_unsigned(t_printf *res, unsigned int n);
void			print_hexa(t_printf *res, unsigned int hex);
void			pad_right_hex(t_printf *res);
void			pad_left_hex(t_printf *res);
void			width_hex(t_printf *res);
void			print_ptr(t_printf *res, unsigned long int ptr);
t_printf		*ft_initialize(t_printf *res);
char			*free_str(char *str);

#endif
