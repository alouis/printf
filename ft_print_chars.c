/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_chars.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 14:06:42 by alouis            #+#    #+#             */
/*   Updated: 2019/12/19 13:49:16 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft/libft.h"

void	width_str(t_printf *res)
{
	if (res->prec < res->str_len && res->prec_flag == 1)
	{
		while (res->width > res->prec)
		{
			ft_putchar(' ');
			res->width--;
			res->len++;
		}
	}
	else
	{
		while (res->width > res->str_len)
		{
			ft_putchar(' ');
			res->width--;
			res->len++;
		}
	}
}

void	pad_right_str(t_printf *res)
{
	if (res->prec <= res->str_len && res->prec >= 0 && res->prec_flag == 1)
	{
		while (res->pad_right > res->prec)
		{
			ft_putchar(' ');
			res->pad_right--;
			res->len++;
		}
	}
	else
	{
		while (res->pad_right > res->str_len)
		{
			ft_putchar(' ');
			res->pad_right--;
			res->len++;
		}
	}
}

void	print_str(t_printf *res, char *s)
{
	if (s == NULL)
	{
		s = ft_strdup("(null)");
		res->mlc = 1;
	}
	res->str_len = (int)ft_strlen(s);
	if (res->width > 0)
		width_str(res);
	if (res->prec_flag == 1 && res->prec < res->str_len && res->prec >= 0)
		res->len = res->len + ft_putnstr(s, res->prec);
	if ((res->prec_flag == 0 || (res->prec_flag == 1 &&
		res->prec >= res->str_len)))
		res->len = res->len + ft_putnstr(s, res->str_len);
	if (res->prec_flag == 1 && res->prec < 0)
		res->len = res->len + ft_putnstr(s, res->str_len);
	if (res->pad_right)
		pad_right_str(res);
	if (res->mlc == 1)
		s = free_str(s);
}

void	print_char(t_printf *res, char c)
{
	if (c == '%' && res->pad_left > 1)
	{
		ft_putnchar('0', (res->pad_left - 1));
		res->len = res->len + (res->pad_left - 1);
	}
	while (res->width > 1)
	{
		ft_putchar(' ');
		res->width--;
		res->len++;
	}
	write(1, &c, 1);
	res->len++;
	while (res->pad_right > 1)
	{
		ft_putchar(' ');
		res->pad_right--;
		res->len++;
	}
}
